
/*

	This settings object is available for re-use anywhere within the 
	scope of this widget.  Avoid hard coding in any names, and just add them
	here instead.
	
*/

var settings = {
	"widgetName": "highchartswidget"
}