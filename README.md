#	New Plugin Widget Template

__INTRODUCTION__: The goal of this project is to simplify the process for creating new chart types as plugins in Sisense.  Sisense has a rhobust plugin framework that allows developers to create additional visualizations and integrate them seamlessly into our platform to look like native functionality.  This process however, requires knowledge of internal APIs in order to work with the plugin framework.  This project aims to highlight the most common aspects of creating new chart types through an example using Highcharts.


__File Structure__: 

*templateHelper*: This is a plugin with some code that is reused by all plugins that originate from this template.  The point is to load this helper one time, and you can copy/paste the template many times to create lots of new plugins.  Each copy of the template can still refer to this templateHelper plugin in order to minimize re-work.

* plugin.json: Registers any code that needs to be accessible to any plugin using the template
* DesignPanel.js: This code adds a directive used by the custom widget's styler panel.  We want to dynamically load the appropriate HTML template and controller for the specific plugin.  This could have been hard-coded into each plugin, but this approach was taken to remove the need for hard-coding.

*highchartswidget*: This is the sample plugin directory, contain all files needed for the plugin.  The file structure is broken down into 3 main sections: designPanel, widget, & resources.
* plugin.json: This file is required by every plugin.  When SisenseWeb loads in your browser, it loops through every folder in the plugins directory and looks for a plugin.json file.  Based on this file, SisenseWeb knows to register the plugin with some basic info (name, etc) and what files are required (javascript & css)
* config.js: This file contains settings that can be referenced from any other file in the plugin.  This is a good place to put labels, titles, etc so that if needed they can be easily changed later on.
* widget: Contains all files relevant to the creation of the new widget type
** widget.js: This is where the actual widget gets registered with the Sisense framework
** widget.css: This CSS file can be used to add styling for your widget
** directive.js: This is the AngularJS directive that loads your widget's HTML template
** template.html: The HTML template for your widget, mostly this file will just be a div container that will later be used by widget.js to render your widget to the DOM.
* designPanel: Contains all files used for managing the Design Panel in the widget editor
** placeholder.html: This is a dummy template that gets loaded automatically, the templateHelper plugin replaces this automatically to use template.html instead and binds it to the controller defined in controller.js
** template.html: This is the HTML seen in the widget editor's Design panel
** controller.js: This is the controller that links the Design Panel's UI to the widget's object model
* resources: This folder contains all additional resources required by the plugin (if any)
** icon-24.png: This is the icon that appears next to the widget's title in the widget editor (widget selection dropdown menu)
** histogram-bellcurve.js: For this sample plugin, highcharts requires an additional module.  Make sure that any additional modules required for your plugin are mentioned in plugin.json

*highchartswidget-empty*: This is a blank version of the sample, for use in training workshops.  All files are exactly the same as in highchartswidget, except some lines in widget/widget.js have been removed for the workshop.

*screenshots*: Any screenshots used by the readme

__Using the Template__: Start by copying the templateHelper plugin from this repository into your Sisense Server's plugins directory.  If you already have the templateHelper installed, no need to copy it a second time.  Next, copy the highchartswidget directory from this repository into your Sisense Server's plugin directory.  

*Plugin Name*: When making your own plugin, you will want to change the name.  Pick a new name (all lowercase) and change the plugin's folder to this name.  You should also update plugin.json's name property, and config.js's widgetName property to match your new plugin name.  

*Additional JavaScript Libraries*: If your plugin requires additional JS files to work, download them to the resources directory and update your plugin.json to include them.

*Widget*: When creating your new widget, you will need to decide what type of data structure is required.  You will need to update the widget/widget.js file in the following places:

![Data Panel](screenshots/data-panel.png)

* data.panels: This determines how many options show up in your widget's data panel, as well as what types are supported (dimensions, measures, both) & how many items can be added to each panel (maxItems)
* data.buildQuery: This function needs to loop through all of your data panels and fetch all the items of each.  Each item contains JAQL items that get populated through the Widget Editor.  The function returns a valid JAQL query with will be sent to the Elasticube via REST API
* data.processresult: This function fires when the JAQL query has returned from the Elasticube.  You should check to ensure that data was actually returned, and convert it from the Sisense dataframe into whatever format your chart expects data.
* render: This function fires when the the chart is about to be rendered onto the page.  Typically all of your formatting settings are defined in this function and then used to render the widget onto the page.  When using Highcharts, this is where you would call the Highcharts() function to render your chart

*Design Panel*: After defining your widget, you likely want to empower Dashboard Designers to make formatting changes to the widget.  This is usually done through the design panel.  The following files will be relevant for enabling this

![Design Panel](screenshots/design-panel.png)

* template.html: This is the HTML used to draw the formatting controls (buttons, sliders, text boxes, radio buttons, etc).  You can use standard AngularJS syntax here to simply the creation of this panel.  The template here is bound to controller.js, so you can reference any variables or functions defined in the controller.
* controller.js: This is the controller for your design panel, which is how your design panel knows the the current settings are as well as how the design panel communicates changes back to the widget's object model.

__NOTES__: 
* This version of the template was designed to work with version 7.1 of Sisense, but should be backwards compatible with prior versions.