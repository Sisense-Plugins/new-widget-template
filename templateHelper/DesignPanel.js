
/*

The new widget template uses renders an html tag as 'customwidgetstyler' for 
the design panel. This helper adds a directive, so that we can load the proper
styler template and controller for the specific widget

*/
mod.directive('customwidgetstyler', [

    function ($timeout, $dom) {

        //  Get the name of the current widget, from the widget editor
        var widgetName = prism.$ngscope.widget.manifest.name;

        //  Use the widget name to figure out the templateUrl and controller path for the styler panel
        return {
            priority: 0,
            replace: false,
            templateUrl: "/plugins/" + widgetName + "/designPanel/template.html",
            controller: "plugin-" + widgetName + ".controllers.stylerController",
            transclude: false,
            restrict: 'E',
            link: function ($scope, element, attrs) {}
        }
    }]);

