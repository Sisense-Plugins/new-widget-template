
//  Panel name defintion
var panels = {
    "values": "Values",
    "filters":"filters"
}

//  Get a reference to the Sisense date/number formatters
var dateFormatter = prism.$injector.get('$filter')('date'),
    numberFormatter = prism.$injector.get('$filter')('numeric');

//  New Chart type registration
prism.registerWidget(settings.widgetName, {
    name: settings.widgetName,
    family: "map",
    title: "Highchart Template",
    iconSmall: "/plugins/" + settings.widgetName + "/resources/icon-24.png",
    styleEditorTemplate: "/plugins/" + settings.widgetName + "/designPanel/placeholder.html",
    hideNoResults: true,
    style: {
        axisScale: "linear",
        pointSize: 1,
        animationSpeed: 1000,
        maxPoints: 10000
    },
    directive: {
        desktop: settings.widgetName
    },
    data: {
        selection: [],
        defaultQueryResult: {},
        panels: [
            {
                name: panels.values,
                type: "visible",
                metadata: {
                    types: ['dimensions'],
                    maxitems: 1
                },
                itemAttributes: ["color"],
                allowedColoringTypes: function(){
                    return {
                        color: true,
                        condition: false,
                        range: true
                    }
                },
                visibility: true
            },
            {
                name: 'filters',
                type: 'filters',
                metadata: {
                    types: ['dimensions'],
                    maxitems: -1
                }
            }
        ],
        //  Allow coloring for the value panel
        canColor: function (widget, panel, item) {
            return (panel.name === panels.values);
        },
        // builds a jaql query from the given widget
        buildQuery: function (widget) {

            // building jaql query object from widget metadata 
            var query = { 
                datasource: widget.datasource, 
                format: "json",
                isMaskedResult:true,
                ungroup: true,
                offset: 0,
                count: widget.style.maxPoints,
                metadata: [] 
            };

            /* Logic to build a query  */



            //  Get the metadata items from the widget's filters panel
            widget.metadata.panel(panels.filters).items.forEach(function(item){

                //  Create a copy of the metadata item
                var newItem = $$.object.clone(item, true);

                //  Since this is a filter, add a scoping property (so jaql knows it a filter)
                newItem.panel = "scope";

                //  Save this to the metadata
                query.metadata.push(newItem);
            })
            
            return query;
        },
        //  Build geojson object from query result
        processResult : function (widget, queryResult) {

            //  raw data to work with
            var rawData = queryResult.$$rows,
                metadata = queryResult.metadata(),
                hasData = rawData.length > 0,
                colors = prism.$ngscope.dashboard.style.options.palette.colors;

            //  Define an object to hold the new results
            var newResults = {
                data: [],
                stats: {}
            };

            /* Logic to format the query result  */

            


            
            //  if we have data, return the map data
            return hasData ? newResults : null;
        }
    },
    render : function (widget, event) {

        //  Find the HTML container element
        var divId = settings.widgetName + '-' + $$get(widget, 'oid',''),
            container = $('#' + divId),
            colors = prism.$ngscope.dashboard.style.options.palette.colors,
            mask = $$get(widget.metadata.panel(panels.values), 'items.0.format.mask', null),
            label = $$get(widget.metadata.panel(panels.values), 'items.0.jaql.title', 'Value');

        // Get widget element
        var element = $(event.element);

        //  Function to format any tooltips
        function valuesTooltip(){
            //  Show formatted number values for real data points
            return '<b>' + label + ': ' + numberFormatter(this.y, mask) + '</b>';
        }

        //  Only run if the div container has already been added
        if (container.length > 0) {

            //  Define the highcharts options
            var options = {
                chart: {
                    height: container.height()
                },
                exporting: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                plotOptions: {
                    bubble: {
                        maxSize: widget.style.pointSize,
                        minSize: widget.style.pointSize
                    },
                    series: {
                        animation: {
                            duration: widget.style.animationSpeed
                        },
                        turboThreshold: widget.style.maxPoints,
                        tooltip: {
                            pointFormatter: valuesTooltip
                        },
                        dataLabels: {
                            enabled: false
                        }
                    }
                },
                series: [
                    {
                        name: 'Bell curve',
                        type: 'bellcurve',
                        xAxis: 1,
                        yAxis: 1,
                        baseSeries: 1,
                        zIndex: -1,
                        color: colors[0],
                        enableMouseTracking: false
                    }, {
                        name: 'Data',
                        type: 'bubble',
                        data: widget.queryResult.data,
                        marker: {
                            radius: 4.5
                        }
                    }
                ],
                xAxis: [
                    {
                        title: {
                            text: 'Data Points'
                        },
                        alignTicks: false
                    }, {
                        title: {
                          text: 'Distribution'
                        },
                        alignTicks: false,
                        opposite: true
                    }
                ],
                yAxis: [
                    {
                        title: { 
                            text: label
                        },
                        type: widget.style.axisScale
                    }, {
                        title: { 
                            text: 'Distribution' 
                        },
                        opposite: true
                    }
                ]
            };

            // allow override highcharts options
 
            var beforeViewLoadedArgs = {widget: widget, options: options, element: element};
 
            widget.trigger('beforeviewloaded', beforeViewLoadedArgs);

            /* Apply formatting and add data to the Highcharts object. */

            //  Render the map
            Highcharts.chart(divId, options);
        }
    },
    destroy : function (widget, args) {},
    options: {
        dashboardFiltersMode: "select",
        selector: false,
        title: false
    },
    sizing: {
        minHeight: 128, //header
        maxHeight: 2048,
        minWidth: 128,
        maxWidth: 2048,
        height: 320,
        defaultWidth: 512
    }
});