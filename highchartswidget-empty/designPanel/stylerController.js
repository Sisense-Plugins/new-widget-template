
/*

    This code defined a controller named 'stylerController' for this plugin
    to allow communication between the design panel's HTML (user input) and
    the widget's object model.  The standard approach is to add a $watch to
    the widget object and add the widget's style property to the controller's 
    scope.  This way, any changes made by the end user will automatically 
    change the relevant style properties of the widget.

*/

mod.controller('stylerController', ['$scope',
    function ($scope) {

        /****************************/
        /* Init any variables       */
        /****************************/


        /****************************/
        /* Add any watches          */
        /****************************/

        //  Watch the widget object
        $scope.$watch('widget', function (val) {

            //  Set the model, from the style
            $scope.model = $$get($scope, 'widget.style');
        });


        /****************************/
        /* Define public methods    */
        /****************************/

        //  Function to simply redraw the widget
        $scope.redraw = function(){
            
            $scope.widget.redraw();
        }
        
        //  Generic function to set a style propery
        $scope.setStyle = function(prop, value){

            //  Apply changes
            $scope.widget.style[prop] = value;

            //  Draw the map again
            $scope.redraw();
        }
    }
]);